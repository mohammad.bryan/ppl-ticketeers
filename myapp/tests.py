from django.test import TestCase, Client
from django.urls import reverse
from django.db import connection
from django.contrib.sessions.backends.db import SessionStore
from supabase import create_client

class LoginViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = SessionStore()
        self.session.save()

        # Create a test user in the database
        cursor = connection.cursor()
        cursor.execute("SET search_path TO public")
        # untuk sekarang pake table auth_user nanti akan diganti
        cursor.execute("INSERT INTO auth_user (username, password, is_superuser, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('testuser', 'password', false, 'test', 'user', 'test@user.com', false, true, '2022-12-27')")
        cursor.close()

    def test_login_get(self):
        # Make a GET request to the login view
        response = self.client.get(reverse("login"))

        # Check that the response has a "200 OK" status code
        self.assertEqual(response.status_code, 200)

        # Check that the response uses the "login.html" template
        self.assertTemplateUsed(response, "login.html")

    def test_login_view_with_no_data(self):
        # Make a GET request to the login view with no data
        response = self.client.get(reverse("login"))

        # Check that the response has a "200 OK" status code
        self.assertEqual(response.status_code, 200)

        # Check that the response uses the "login.html" template
        self.assertTemplateUsed(response, "login.html")

        # Check that the response contains the login form
        self.assertContains(response, '<form method="post">')
        self.assertContains(response, '<input type="username" name="username"')
        self.assertContains(response, '<input type="password" name="password"')
        self.assertContains(response, '<button type="submit" class="btn btn-primary btn-lg" style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>')


    def test_login_post_invalid(self):
        # Attempt to login with invalid credentials
        response = self.client.post(reverse("login"), {"username": "testuser", "password": "wrongpassword"})

        # Check that the user is redirected to the bye page
        self.assertRedirects(response, reverse("bye"))

    def test_login_post_valid(self):
        # Attempt to login with the test user
        response = self.client.post(reverse("login"), {"username": "testuser", "password": "password"})

        # Check that the user is redirected to the index page
        self.assertRedirects(response, reverse("index"))

        # Check that the username is stored in the session
        self.assertEqual(self.client.session.get("username"), "testuser")

    def test_template(self):
        response = self.client.get(reverse('login'))
        self.assertTemplateUsed(response, 'login.html')
        self.assertContains(response, '<h1 class="text-center">Welcome back!</h1>')
        self.assertContains(response, '<input type="username" name="username"')
        self.assertContains(response, '<input type="password" name="password"')
        self.assertContains(response, '<button type="submit" class="btn btn-primary btn-lg" style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>')


