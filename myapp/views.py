from django.db import connection
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib import messages

# Create your views here.
def index(request):
    return render(request, 'base.html')

def login(request):
    cursor = connection.cursor()
    cursor.execute("SET search_path TO public")


    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        cursor.execute("SELECT username FROM auth_user WHERE username = %s", [username])
        account = cursor.fetchone()

        cursor.execute("SELECT username, password FROM auth_user WHERE username = %s AND password = %s", [username, password])
        if cursor.fetchone() is not None:
            cursor.execute("SET search_path TO public")
            request.session['account'] = account
            request.session['username'] = username
            return HttpResponseRedirect("/index")
        else:
            return HttpResponseRedirect("/bye")

    return render(request, "login.html")

def bye(request):
    return render(request, 'bye.html')



