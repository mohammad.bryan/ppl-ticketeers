from django.test import TestCase, Client
from django.urls import reverse
from django.db import connection
from django.contrib.sessions.backends.db import SessionStore
from supabase import create_client

class ViewListingTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = SessionStore()
        self.session.save()

        # Create a test listing in the database
        with connection.cursor() as cursor:
            cursor.execute("SET search_path TO public")
            cursor.execute("INSERT INTO listing (sellername, title, status) VALUES ('Test Seller', 'Test Listing', 'available')")
    
    def test_view_listing(self):
        # Retrieve the test listing ID
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM listing WHERE sellername = 'Test Seller'")
            result = cursor.fetchone()
            listing_id = result[0]
        
        # Access the viewlisting endpoint with the test listing ID
        response = self.client.get(reverse("viewlisting", args=[listing_id]))

        # Check that the response is successful
        self.assertEqual(response.status_code, 200)

        # Check that the sellername, title, and status are correctly displayed in the HTML response
        self.assertContains(response, "Test Seller")
        self.assertContains(response, "Test Listing")
        self.assertContains(response, "available")
