from django.shortcuts import render
from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.db import connection
from collections import namedtuple

def tuple_fetch(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.
def viewlisting(request, id):
    cursor = connection.cursor()
    cursor.execute("SET search_path TO public")
    cursor.execute("SELECT * FROM listing where id = " + str(id) + ";")
    result = []
    result = tuple_fetch(cursor)
    cursor.close()
    
    sellername = result[0][1]
    title = result[0][2]
    status = result[0][6]
    print(sellername)
    print(title)
    print(status)

    return render(request, 'detail.html', {"sellername" : sellername, "title" : title, "status" : status})