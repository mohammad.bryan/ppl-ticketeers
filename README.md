# PPL ASSIGNMENT

## Website
> Production: <https://ppl-ticketeers-git-main-mohammadbryan-uiacid.vercel.app/>

## Sonarqube
[![Quality Gate Status](https://sonarqube.cs.ui.ac.id/api/project_badges/measure?project=Ticketeers-MohammadBryanMahdavikhia&metric=alert_status)](https://sonarqube.cs.ui.ac.id/dashboard?id=Ticketeers-MohammadBryanMahdavikhia)

## Pipeline
> Main  
    [![main pipeline status](https://gitlab.com/mohammad.bryan/ppl-ticketeers/badges/main/pipeline.svg)](https://gitlab.com/mohammad.bryan/ppl-ticketeers/-/commits/main)
